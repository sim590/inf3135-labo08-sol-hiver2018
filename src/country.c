/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "treemap.h"

#define BUF_LEN 256

int main(int argc, char *argv[]) {
    char buf[BUF_LEN];

    TreeMap map = treemapCreate();
    while (fgets(buf, BUF_LEN, stdin) != NULL) {
        if (buf[BUF_LEN-1] != '\n') {
            // Ignorer les lignes ne faisant pas moins de BUF_LEN caractères.
            char c;
            while ((c = getchar()) != '\n') { }
        }

        char* key = strtok(buf, ",");
        char* d = strtok(NULL, "\n");
        // Il se peut que `d` soit NULL dans le cas où le pays ne possède pas de
        // capitale. Il existe une telle occurrence dans le cas de
        // l'antarctique. Bien que l'antarctique ne soit pas un pays, le fichier
        // de données semblait le lister ainsi...
        //
        // Par conséquent, on utilise l'opérateur ternaire (bool ? r1 : r2).
        // où "r" signifie une r-valeur.
        //
        // https://eli.thegreenplace.net/2011/12/15/understanding-lvalues-and-rvalues-in-c-and-c
        treemapSet(&map, key, d ? d : "n/a");
    }

    char* keys[4] = {"AGO", "AND", "BLM", "CAN"};
    printf("Quelques exemples d'accès aux données stockées...\n");
    for (size_t i = 0; i < 4; ++i) {
        printf("%s -> %s\n", keys[i], treemapGet(&map, keys[i]));
    }

    return 0;
}

/* vim:set et sw=4 ts=4 tw=120: */

