// Types
// -----


typedef struct {
    struct TreeNode *root; // Pointeur vers la racine
} TreeMap;

// Prototypes
// ----------

TreeMap treemapCreate();
char *treemapGet(const TreeMap *t, char *key);
void treemapSet(TreeMap *t, char *key, char *value);
bool treemapHasKey(const TreeMap *t, char *key);
void treemapPrint(const TreeMap *t);
void treemapDelete(TreeMap *t);

/* vim: set ts=4 sw=4 tw=120 et :*/

