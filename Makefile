CC      := gcc
CFLAGS  := -c $(if DEBUG,-g,)
CFILES  := src/treemap.c src/country.c
OBJECTS := $(CFILES:.c=.o)

.PHONY: bindir

all: bin/country

bin/country: $(CFILES:.c=.o)
	mkdir -p bin
	$(CC) -o $@ $^

%.o: %.c
	$(CC) -o $@ $(CFLAGS) $<

clean:
	rm -f $(CFILES:.c=.o)

#  vim: set ts=4 sw=4 tw=80 noet :

