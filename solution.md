## Solutions

## 1 - Programmation modulaire

> L'objectif de cette question est de construire une petite application
> décomposée en modules :
>
> * En utilisant des programmes Unix (moi, je l'ai fait avec seulement `grep` et
>   `sed`), récupérez le nom de toutes les capitales stockées dans le fichier
>   `countries.json` disponible dans le projet Github
>   [countries](https://github.com/mledoze/countries/) et stockez le résultat
>   dans un fichier texte nommé `capitales.txt`. Au besoin, n'hésitez pas à
>   modifier le fichier manuellement, par exemple pour éliminer certains noms
>   avec des caractères spéciaux ou des lignes vides, l'objectif n'étant pas
>   d'avoir un résultat parfait, mais un résultat approximatif. Ajoutez également
>   le code de trois lettres du pays correspondant à la capitale (dans le fichier
>   `countries.json`, ces codes sont identifiés par la chaîne `cca3`).
>   Sauvegardez le résultat dans un fichier texte `countries.txt`, pour que les
>   premières lignes ressemblent à
>   ```
>   ABW,Oranjestad
>   AFG,Kabul
>   AGO,Luanda
>   AIA,The Valley
>   ...
>   ```

On peut premièrement faire une observation du filtre que fait `grep` sur le
[fichier][countries]:

```
$ grep cca3 countries.json
		"cca3": "ABW",
		"cca3": "AFG",
		"cca3": "AGO",
		"cca3": "AIA",
		"cca3": "ALA",
...
```

Cela donne toutes les lignes contenant la chaîne de caractère `cca3`. Il est
possible de faire de même avec `countries`. Ainsi, on peut continuer en
retirerant les caractères qu'on ne souhaite pas garder:

```
$ grep cca3 countries.json | sed -e 's/^.*: "\(.*\)"/\1/g'
ABW,
AFG,
AGO,
AIA,
ALA,
...
```

- `-e`: commande à exécuter sur le fichier;
- `-e 's/^.*: "\(.*\)"/\1/g'` exécuter la commande `s` (recherche et
  remplacement). La barre oblique `/` sert de délimiteur pour la commande (voir
  `sed(1)`). On remplace donc le motif `^.*: "\(.*\)"` par le contenu trouvé
  entre les parenthèses `\(.*\)`.
  - `^` indique le début de la ligne;
  - `.` indique n'importe quel caractère;
  - `*` est un quantificateur pour *0 ou plus*;
  - `: ` est simplement le caractère `:` suivi d'un espace;
  - `"\(.*\)"` est un motif avec mémoire. Le programme se souviendra de ce qui
    se trouvait entre les guillemets et le stockera dans la variable `\1`. Cela
    permet de retrouver `"ABW"`, `"AFG"`, etc. Ainsi, on remplace le tout par
    `\1`.

Ceci donne la première partie de la solution. Nous devons faire une chose
semblable avec l'autre partie puisque la sortie suivante montre qu'on a encore
du travail à faire:

```
$ grep capital countries.json
		"capital": ["Oranjestad"],
		"capital": ["Kabul"],
		"capital": ["Luanda"],
		"capital": ["The Valley"],
		"capital": ["Mariehamn"],
...
```

On procède donc avec:

```
$ grep capital countries.json | sed -e 's/^.*\[\(.*\)\],/\1/g' -e 's/"//g'
Oranjestad
Kabul
Luanda
The Valley
Mariehamn
...
```

Il reste donc à joindre les deux solutions ensemble. Pour ce faire, on peut
utiliser `paste(1)`:

```
$ paste -d "" \
        <(grep cca3 countries.json | sed -e 's/^.*: "\(.*\)"/\1/g') \
        <(grep capital countries.json | sed 's/^.*\["\(.*\)"\],/\1/g')
AFG,Kabul
AGO,Luanda
AIA,The Valley
ALA,Mariehamn
```

**N.B**: les barres obliques inversées à la fin des lignes permettent seulement
de mettre la commande `bash` sur plusieurs lignes.

Regardez ce que l'option `-d` fait dans `paste(1)`.

[countries]: https://raw.githubusercontent.com/mledoze/countries/master/countries.json

>
> * Reprenez l'exemple de la structure de données `Treemap` présentée en classe
>   et disponible dans le répertoire `exemples` du dépôt des exercices. et
>   transformez-le en module avec une interface `treemap.h` et une implémentation
>   séparée `treemap.c`.

Voir les fichiers `src/treemap.c` et `src/treemap.h`.

> * Compilez manuellement le module `treemap`.
> * Concevez un autre module (sans interface) d'un seul fichier `country.c` qui
>   contient une fonction `main` et qui stocke les données calculées plus haut
>   dans une instance de `Treemap` (les clés sont les codes de 3 lettres et les
>   valeurs sont les capitales).

Voir le fichier `src/country.c`.

Le programme lit son entrée standard en attendant un fichier de la forme
suivante:

    ABW,Oranjestad
    AFG,Kabul
    AGO,Luanda
    AIA,The Valley
    ...

Les lignes dépassant 256 caractères sont omises.

> * Incluez un Makefile manuel permettant de compiler automatiquement les 2
>   modules.
> * Rendez le Makefile précédent plus générique.

Voir le fichier `Makefile`.

## 2 - La bibliothèque Jansson

> Refaites l'exercice précédent en utilisant la bibliothèque
> [Jansson](http://www.digip.org/jansson/). Il s'agit d'un bon exercice de
> refactorisation de votre code. Plus précisément:
>
> 1. `country.c` utilise actuellement la liste produite par grep.
> 2. Assurez-vous que tout fonctionne en sauvegardant quelques sorties console
>    dans des fichiers.
> 3. Modifiez `country.c` pour lire depuis un fichiers JSON avec Jansson.
> 4. Vérifiez que rien n'est brisé en faisant des `diff`s avec les fichiers
>    produits à l'étape 2.

Naviguez sur la branche `jansson` afin de tester. Le travail a été validé
principalement dans a3a6b61.

<!-- vim: set ts=4 sw=4 tw=80 et :-->

